package org.tds.sgh.business;

public enum EstadoReserva {
	
	PENDIENTE{
		@Override
		public String toString() {
			return "Pendiente";
		}
	}, 
	TOMADA{
		@Override
		public String toString() {
			return "Tomada";
		}
	}, 
	FINALIZADA{
		@Override
		public String toString() {
			return "Finalizada";
		}
	}, 
	CANCELADA{
		@Override
		public String toString() {
			return "Cancelada";
		}
	}, 
	NO_TOMADA
	{
		@Override
		public String toString() {
			return "No Tomada";
		}
	};
	
	
	

}
