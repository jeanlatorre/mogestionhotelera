package org.tds.sgh.helper;

import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;

public class DateHelper {

	static SimpleDateFormat fmt = new SimpleDateFormat("dd-MMM-yyyy");
	public static String format(GregorianCalendar date){

	
       fmt.setCalendar(date);
        String dateFormatted = fmt.format(date.getTime());
        return dateFormatted;
    }

	
	
}
